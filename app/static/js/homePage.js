var homePage = (function () {

	var home = function (ctx, next) {
		console.log("`home` function called.");
		$.ajax({
			method: 'GET',
			url: '/static/templates/index.hbs',
			dataType: 'html',
			success: function (response) {
				scroll(0, 0);
				var compiledTemplate = Handlebars.compile(response);
				var renderedHtml = compiledTemplate();
				var dom = document.getElementById("page-data");
				dom.innerHTML = renderedHtml;
				next();
                $('#customizedUnit').dropdown({
                    onChange: function(text, value, $selectedItem){
                        addCustomizedUnitOptions(text, value);
                    }
                });
			},
			async: false
		});
	};

	var homeP = {};
	homeP.home = home;
	return homeP;

})();
