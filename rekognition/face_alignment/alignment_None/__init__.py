"An alignment instance that does nothing except resizing"

import cv2

from ..base_alignment import FaceAlignment

class NoAlignment(FaceAlignment):

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def _landmarkDetection(self, image, bboxes):
        '''
        Unused
        '''
        pass

    def _align(self, images, bboxes):
        '''
        Resized the image to a fixed size.
        '''
        aligned_images = [] 

        for i in range(len(images)):
            image_faces = []
            for j in range(len(bboxes[i])):
                x,y,w,h = bboxes[i][j]
                final_image = cv2.resize(images[i][int(y):int(y+h),int(x):int(x+w),:], (112,112))
                image_faces.append(final_image)
            aligned_images.append(image_faces)

        return aligned_images
