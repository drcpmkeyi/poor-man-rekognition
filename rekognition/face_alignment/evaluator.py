"""Implementation of ``FaceAlignmentEvaluator`` class."""

import os

import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import simps

class FaceAlignmentEvaluator:
    r"""Evaluate face landmark detections for a database of faces. Implemented
    to take into account any generic dataset. 
       """

    def __init__(self, true_landmarks, pred_landmarks, **kwargs):
        """

        :param true_landmarks:
            list of :math:`N` sublists, each sublist containing the landmarks locations
            :math:`(x,y)` of dimensions :math:`(68, 2)`. :math:`N` is the number of
            faces.
        :type true_landmarks: ``list``

        :param pred_landmarks:
            list of :math:`N` sublists, each sublist containing the landmarks predictions
            :math:`(x,y)` of dimensions :math:`(68, 2)`. :math:`N` is the number of
            faces.
        :type pred_landmarks: ``list``

        :param \**kwargs:
            See below

        :Keyword Arguments:
            * **normalization** (``string``) --
                Normalization distance set to one of the following:

                * **corners** (default): distance between the corners of the two eyes.
                * **diagonal**: distance between the corners of the bounding box of the landmarks.
                * **pupil**: distance between the pupil of both eyes.

            * **failure_threshold** (``float``) --
                Threshold for error above which the landmark prediction is a failure.
                Default is 0.08.

            * **ced_step** (``float``) --
                Step size of errors while creating the Cumulative Error Distribution plot.
                Default is 0.001.
            
            * **no_plot** (``bool``) --
                Set to `True` if plotting CED Curve is not required. Default is `False`.

            * **plot_dest** (``string``) --
                Set to some destination to save the plot. Not saved by default.

            * **plot_title** (``string``) --
                Give title to the CED Curve plot. 

        """

        assert(len(true_landmarks) == len(pred_landmarks))
        self.true_landmarks = true_landmarks 
        self.pred_landmarks = pred_landmarks

        self.normalization = kwargs.get('normalization', 'corners')
        self.failure_threshold = kwargs.get('failure_threshold', 0.08)
        self.ced_step = kwargs.get('ced_step', 0.0001) 

        self.no_plot = kwargs.get('no_plot', False)
        self.plot_dest = kwargs.get('plot_dest', None)
        self.plot_title = kwargs.get('plot_title', '---')

    def _norm_factor(self, landmarks):
        """
        Obtain the normalization factor value for the landmarks.

        :param landmarks:
            Ground truth landmarks for a particular face.
        :type landmarks: ``list``

        :Returns:

            (``float``): normalization factor
        """
        if self.normalization == 'corners':
            return np.linalg.norm(landmarks[36] - landmarks[45])
        if self.normalization == 'diagonal':
            height, width = np.max(landmarks, axis=0) - np.min(landmarks, axis=0)
            return np.sqrt(height ** 2 + width ** 2)
        if self.normalization == 'pupil':
            return np.linalg.norm(np.mean(landmarks[36:42], axis=0) - np.mean(landmarks[42:48], axis=0))

    def _image_landmark_error(self, true_landmarks, pred_landmarks):
        """
        Obtain the landmarks error for single face

        :param true_landmarks:
            list containing the landmarks locations :math:`(x,y)` of dimensions 
            :math:`(68, 2)`. :math:`N` is the number of faces.
        :type true_landmarks: ``list``

        :param pred_landmarks:
            list containing the landmarks predictions :math:`(x,y)` of dimensions 
            :math:`(68, 2)`. :math:`N` is the number of faces.
        :type pred_landmarks: ``list``


        :Returns:

            (``float``): error for the given face landmarks
        """
        
        normFactor = self._norm_factor(true_landmarks)
        error = np.mean(np.sqrt(np.sum((true_landmarks - pred_landmarks)**2, axis=1)))  
        return error / normFactor

    def landmark_error(self, true_landmarks, pred_landmarks):
        """

        :param true_landmarks:
            list of :math:`N` sublists, each sublist containing the landmarks locations
            :math:`(x,y)` of dimensions :math:`(68, 2)`. :math:`N` is the number of
            faces.
        :type true_landmarks: ``list``

        :param pred_landmarks:
            list of :math:`N` sublists, each sublist containing the landmarks predictions
            :math:`(x,y)` of dimensions :math:`(68, 2)`. :math:`N` is the number of
            faces.
        :type pred_landmarks: ``list``

        :Returns:

            (``list``): containing error for each image.

            (``float``): average error for all the images
        """
        errors = []

        for i in range(len(true_landmarks)):
            error = self._image_landmark_error(true_landmarks[i], pred_landmarks[i])
            errors.append(error)

        errors = np.array(errors)
        average_error = np.mean(errors)

        return errors, average_error

    def plot_ced_curve(self, ced, xaxis):
        r"""
        Plot CED Curve using ``matplotlib``. Saves or shows the plot depending on ``plot_dest``.

        :param ced:
            List containing the cumulative error values.
        :type ced: ``list``

        :param xaxis:
            The error threshold values corresponding to ``ced``.
        :type xaxis: ``list``

        """

        fig = plt.figure(0, dpi=200)
        plt.title('CED Curve : ' + self.plot_title)
        plt.axis([0.0, self.failure_threshold, 0, 1])
        plt.xlabel('Error')
        plt.ylabel('Cumulative fraction of faces')
        plt.plot(xaxis, ced)
        if self.plot_dest is None:
            plt.show()
        else:
            plt.savefig(os.path.join(self.plot_dest, 'CED-Curve_' + self.plot_title + '.png'))
        plt.close(fig)

    def _generate_ced_curve(self, errors):
        """
        Generate CED curve of the errors.
        The plot is printed if `self.no_plot` is `False`.

        :param errors:
            list containing errors for each face.
        :type errors: ``list``

        :Returns:

            **failure_rate** (``float``)
                The failure rate of the detections

            **area_under_curve** (``float``)
                AUC of the CED curve.
        """
        range_values = np.arange(0, self.failure_threshold + self.ced_step, self.ced_step)
        ced =  [float(np.count_nonzero([errors <= x])) / len(errors) for x in range_values]

        failure_rate = 1 - ced[-1]
        area_under_curve = simps(ced, x=100*range_values) / self.failure_threshold

        if self.no_plot is False:
            self.plot_ced_curve(ced, range_values)

        return failure_rate, area_under_curve
        
    def evaluate(self):
        r"""
        Compute the quantification errors and metrics and plots the CED curve.

        :Returns:

            **quantified_scores** (``dict``)
               Dictionary containing values of the various quantifying metrics.
        """
        
        quantified_scores = {}

        all_errors, average_error = self.landmark_error(self.true_landmarks, self.pred_landmarks)
        quantified_scores['average_error'] = average_error

        failure_rate, auc = self._generate_ced_curve(all_errors)
        quantified_scores['failure_rate'] = failure_rate
        quantified_scores['area_under_curve'] = auc

        return quantified_scores
