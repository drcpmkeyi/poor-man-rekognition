'''Implementation of KazemiSullivan Face Alignment using dlib in KazemiSullivan class'''

import dlib
import cv2
import numpy as np

from ..base_alignment import FaceAlignment
from .template_data import MINMAX_TEMPLATE

class KazemiSullivan(FaceAlignment):
    r'''
    Performs face alignment using Kazemi-Sullivan using dlib.           
    '''

    def __init__(self, model, **kwargs):
        '''
        Initializes the landmarks detector.

        :param model:
            Path to Kazemi Sullivan model.
        :type thresh: ``string``

        :param \**kwargs:
            See ``base_alignment`` ``__init__()`` parameters
        '''
        super().__init__(**kwargs)

        self.detector = dlib.shape_predictor(model) 

        self.landmark_indices = [39, 42, 57]

    def _toDlibBbox(self, bboxes, single=False):
        '''
        Converts a bbox of :math:`(x,y,w,h)` format to that
        of dlib's ``rectangle`` class.

        :param bboxes:
            List of bounding boxes. The outer list should be
            removed if ``single`` is True.
        :type bboxes: ``list``

        :param single:
            True if only bounding box is passed as ``bboxes``.
            Default is False.

        :Returns:

            (``list``): ``rectangle`` format bounding boxes.
        '''
        if single:
            bboxes = [bboxes]
        final_bboxes = []

        for bbox in bboxes:
            rectangle = dlib.rectangle(
                    int(bbox[0]),
                    int(bbox[1]),
                    int(bbox[0] + bbox[2]),
                    int(bbox[1] + bbox[3])
                    )
            final_bboxes.append(rectangle)

        if single and len(bboxes) == 1:
            final_bboxes = final_bboxes[0]

        return final_bboxes

    def _fromDlibShape(self, shape):
        '''
        Converts a dlib ``shape`` of landmark detections to
        numpy format.

        :param shape:
            Dlib's ``shape`` object.
        :type shape: ``dlib::shape``

        :Returns:
            
            (``np.array``): Numpy array of predictions of
            dimensions :math:`(68, 2)`.
        '''
        landmarks = np.empty((68,2))
        for i in range(68):
            landmarks[i] = [shape.part(i).x, shape.part(i).y]
        return landmarks

    def _landmarkDetection(self, images, bboxes):
        '''
        Computes the face landmark detections.

        See ``base_alignment::_landmarkDetection`` for more details.
        '''

        final_landmarks = []

        for i in range(len(images)):
            image = cv2.cvtColor(images[i], cv2.COLOR_RGB2GRAY)
            image_landmarks = []
            image_bboxes = self._toDlibBbox(bboxes[i])
            
            for j in range(len(image_bboxes)):
                shape = self.detector(image, image_bboxes[j])
                np_shape = self._fromDlibShape(shape)
                image_landmarks.append(np_shape)
            
            final_landmarks.append(image_landmarks)
        return final_landmarks

    def _single_align(self, image, landmark):
        '''
        Aligns a single face given its landmark locations

        :Returns:
            (``cv2.image``): RGB aligned face    
        '''
        H_mat = cv2.getAffineTransform(landmark[self.landmark_indices].astype(np.float32),
                                   self.align_dim * MINMAX_TEMPLATE[self.landmark_indices].astype(np.float32) * 1.0)
        thumbnail = cv2.warpAffine(image, H_mat, (self.align_dim, self.align_dim))
        return thumbnail
         

    def _align(self, images, bboxes):
        '''
        Aligns the faces.

        See ``base_alignment::_align`` for more details.
        '''

        landmarks = self._landmarkDetection(images, bboxes)
        aligned_images = [] 

        for i in range(len(images)):
            image_faces = []
            for j in range(len(bboxes[i])):
                x,y,w,h = bboxes[i][j]
                final_image = self._single_align(images[i], landmarks[i][j])
                image_faces.append(final_image)
            aligned_images.append(image_faces)

        return aligned_images
