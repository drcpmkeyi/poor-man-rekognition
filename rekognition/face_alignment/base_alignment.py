"""Contains the FaceAlignment abstract class for performing any Face
Alignment."""

from abc import abstractmethod, ABC
import cv2
import numpy as np

class FaceAlignment(ABC):

    """
    Contains the abstract methods for performing any face alignment. Make sure
    to inherit this class when adding a new alignment method.

    .. note::
        Any detector (say ``xyz``) must be present in a ``alignment_xyz`` directory,
        wherein all files pertaining to the detector must be kept.
    """

    def __init__(self, batch_size=10, align_dim=112):
        '''

        :param batch_size:
            Batch size (number of images) to be processed in a single run. If a list
            of image names are passed, only ``batch_size`` amount will be read at a
            time. Default: 10.
        :type batch_size: ``list``

        '''
        self.batch_size = batch_size 
        self.align_dim = align_dim

    def landmarkDetection(self, src_images, bboxes):
        '''
        Calculates the landmarks for given images and their bounding boxes.

        :param src_images:
            ``src_images`` is a list of :math:`N` images (each image in RGB order).
        :type src_images: ``list``
            
        :param bboxes:
            list of :math:`N` sublists (for each image), with each sublist containing 
            some :math:`i` sublists with four values denoting a face's bounding box
            for the corresponding image.
        :type bboxes: ``list``

        :Returns:

            (``list``): list of :math:`N` sublists with each sublist containing the
            :math:`(68, 2)` landmark detections for each bounding box.
        '''
        # src_images must be a list/tuple of filenames or images 
        assert(isinstance(src_images, (list, tuple)))

        detections = []
        offset = 0

        while offset < len(src_images):
            images = []
            images_bboxes = []
            lim = min(self.batch_size, len(src_images)-offset)
            for i in range(lim):
                images.append(src_images[offset+i])
                images_bboxes.append(bboxes[offset+i])
            detections.extend(self._landmark_detection(images, images_bboxes))
            offset += lim 

        return detections

    @abstractmethod
    def _landmarkDetection(self, images, bboxes):
        '''
        Abstract method to compute landmarks for given images and bounding boxes.
        For parameters definition, see ``self.landmarkDetection()``.
        '''
        pass

    def align(self, src_images, bboxes):
        '''
        Align images given their bounding boxes

        :param src_images:
            ``src_images`` is a list of :math:`N` images (each image in RGB order).
        :type src_images: ``list``
            
        :param bboxes:
            list of :math:`N` sublists (for each image), with each sublist containing 
            some :math:`i` sublists with four values denoting a face's bounding box
            for the corresponding image.
        :type bboxes: ``list``

        :Returns:

            (``list``): list of :math:`N` sublists with each sublist containing the
            aligned face for each bounding box.
        '''
        assert(isinstance(src_images, (list, tuple)))

        aligned_images = []
        offset = 0

        while offset < len(src_images):
            images = []
            images_bboxes = []
            lim = min(self.batch_size, len(src_images)-offset)
            for i in range(lim):
                images.append(src_images[offset+i])
                images_bboxes.append(bboxes[offset+i])
            aligned_images.extend(self._align(images, images_bboxes))
            offset += lim 

        return aligned_images

    @abstractmethod
    def _align(self, src_images, bboxes):
        '''
        Abstract method to align images given the bounding boxes.
        For parameters definition, see ``self.align()``.
        '''
        pass
