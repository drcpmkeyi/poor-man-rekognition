from .alignment_KazemiSullivan import KazemiSullivan
from .alignment_None import NoAlignment
from .evaluator import FaceAlignmentEvaluator

AlignmentHashMap = {
    "kazemiSullivan": KazemiSullivan,
    "None": NoAlignment
}
