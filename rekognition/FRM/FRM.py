'''This is the implementation of FRM: the Face Recognition Module'''

import numpy as np
import cv2

from ..face_detection import DetectorHashMap
from ..face_alignment import AlignmentHashMap
from ..face_identification import FaceIdentifier, EmbedderHashMap

class FRM:

    '''
    Wraps around the selection of the various submodules of face detection, 
    alignment and embedding, as well as providing simple interfaces for training
    and testing the module.
    '''

    def __init__(self, detector_data=None, alignment_data=None, embedder_data=None):
        '''
        Initialize with default submodules, if not supplied.

        :param detector_data:
            FaceDetector object name and args as a dict
        :type detector_data: ``dict``

        :param alignment_data:
            FaceAlignment object name and args as a dict
        :type alignment_data: ``dict``

        :param embedder_data:
            FaceEmbedder object name and args as a dict
        :type embedder_data: ``dict``
        '''
        self.initFaceDetector(detector_data)
        self.initFaceAlignment(alignment_data)
        self.initFaceEmbedder(embedder_data)
        
    def initFaceDetector(self, detector_data):
        '''
        Change face detector

        :param detector_data:
            FaceDetector object name and args as a dict
        :type detector_data: ``dict``
        '''
        if detector_data is None:
            self.detector = DetectorHashMap['faceBoxes']()
        else:
            self.detector = DetectorHashMap[detector_data['name']](**detector_data['args'])

    def initFaceAlignment(self, alignment_data):
        '''
        Change face alignment method

        :param alignment_data:
            FaceAlignment object name and args as a dict
        :type alignment_data: ``dict``
        '''
        if alignment_data is None:
            self.alignment = AlignmentHashMap['kazemiSullivan'](model='shape_predictor_68_face_landmarks.dat')
        else:
            self.alignment = AlignmentHashMap[alignment_data['name']](**alignment_data['args'])

    def initFaceEmbedder(self, embedder_data):
        '''
        Change face embedder 

        :param embedder_data:
            FaceEmbedder object name and args as a dict
        :type embedder_data: ``dict``
        '''
        if embedder_data is None:
            self.embedder = EmbedderHashMap['mobileFaceNet']()
        else:
            self.embedder = EmbedderHashMap[embedder_data['name']](**embedder_data['args'])

    def train(self, src_faces, labels):
        '''
        Trains on the given face images and labels

        :param src_faces:
            list of faces (images)
        :type src_faces: ``list``
        '''
        bboxes, _ = self.detector.detect(src_faces)

        formatted_bboxes = [[[2,4,2,4]] for i in range(len(src_faces))]

        for i in range(len(bboxes)):
            formatted_bboxes[bboxes[i][0]] = [bboxes[i][1]]

        aligned_faces = self.alignment.align(src_faces, formatted_bboxes)
        aligned_faces = [aligned_faces[i][0] for i in range(len(src_faces))]
        self.identifier = FaceIdentifier(aligned_faces, labels, self.embedder)

    def test(self, src_images):
        '''
        Tests on the given images

        :param src_images:
            list of images
        :type src_images: ``list``

        :Returns:
            (``list``) containing bounding boxes across images and their
            respective labels, if any
        '''
        bboxes, _ = self.detector.detect(src_images)

        formatted_bboxes = [[] for i in range(len(src_images))]

        for i in range(len(bboxes)):
            formatted_bboxes[bboxes[i][0]].append(bboxes[i][1])

        aligned_faces = self.alignment.align(src_images, formatted_bboxes)

        detected_labels = []

        for i in range(len(src_images)):
            if len(aligned_faces[i]) > 0:
                partition_labels = self.identifier.test(aligned_faces[i])
                detected_labels.append([partition_labels, formatted_bboxes[i]])
            else:
                detected_labels.append([[], []])
                    
        return detected_labels
