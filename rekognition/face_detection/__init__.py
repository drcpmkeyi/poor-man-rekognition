"""``face_detection`` contains the various face detection classes and the 
``FaceDetectorEvaluator`` class."""

from .detector_ViolaJones import ViolaJones
from .detector_FaceBoxes import FaceBoxes
from .evaluator import FaceDetectorEvaluator

DetectorHashMap = {
    "violaJones": ViolaJones,
    "faceBoxes": FaceBoxes,
}
