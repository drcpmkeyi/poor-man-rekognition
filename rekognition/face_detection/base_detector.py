"""Contains the FaceDetector abstract class for building any Face
Detector."""

from abc import abstractmethod, ABC
import cv2
import numpy as np

class FaceDetector(ABC):

    """ Contains the abstract methods for building any face detector. Make sure
    to inherit this class when adding a new detector.

    .. note::
        Any detector (say ``xyz``) must be present in a ``detector_xyz`` directory,
        wherein all files pertaining to the detector must be kept.
    """


    def __init__(self, batch_size=10):
        '''
        :param batch_size:
            Batch size (number of images) to be processed in a single run. If a list
            of image names are passed, only ``batch_size`` amount will be read at a
            time. Default: 10.
        :type batch_size: ``list``
        '''
        self.batch_size = batch_size 

    def detect(self, src_images):
        '''
        Public method to perform detection of all images in ``src_images``. Currently
        only the detections are returned for every detector without the scores. If there is
        a sufficient reason, even scores could be returned later.

        :param src_images:
            ``src_images`` must either be a list of image file names (string) or a
            list of images (each image in RGB order).
        :type src_images: ``list``
        '''

        # src_images must be a list/tuple of filenames or images 
        assert(isinstance(src_images, (list, tuple)))

        if not isinstance(src_images[0], str):
            return self._detect(src_images)

        offset = 0
        detections = []

        while offset < len(src_images):
            images = []
            lim = min(self.batch_size, len(src_images)-offset)
            for i in range(lim):
                images.append(cv2.cvtColor(cv2.imread(src_images[offset+i]), cv2.COLOR_BGR2RGB))
            detections.extend(self._detect(images, offset))
            offset += lim 

        return detections

    @abstractmethod
    def _detect(self, images, offset=None):
        '''
        Method to be defined for performing face detection on a list of images.

        :param images:
            list of images to perform detection in this call.
        :type images: ``list``

        :param offset:
            The file number of the first image. Default is ``None``.
        :type offset: ``int`` 
        '''
        pass

    @abstractmethod
    def _single_detect(self, image):
        '''
        Method to be defined for performing face detection on a single image.

        :param image:
            image to perform detection on
        :type images: Preferably OpenCV read image
        '''
        pass
