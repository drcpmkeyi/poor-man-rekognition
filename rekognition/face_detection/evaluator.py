"""Implementation of ``FaceDetectorEvaluator`` class."""

import os

import numpy as np
import matplotlib.pyplot as plt

from .bbox import bbox_overlaps

class FaceDetectorEvaluator:
    r"""Evaluate one-class (eg. face) object detections for a generic
    dataset. Implemented to take into account WIDER FACE's detections. :math:`N` is the number
    of images in the dataset, and :math:`N[i]` the number of faces in the :math:`i^{th}` image.

       """

    def __init__(self, true_bboxes, pred_bboxes, pred_scores, **kwargs):
        r"""
        
        :param true_bboxes:
            list of :math:`N` sublists, each element a ground truth
            bounding box specification of the form :math:`[x,y,w,h]` in the :math:`i^{th}` image.
        :type true_bboxes: ``list``

        :param pred_bboxes:
            list of :math:`N` sublists, each element a predicted
            bounding box specification of the form :math:`[x,y,w,h]` in the :math:`i^{th}` image. They
            must be provided in descending order of scores as provided by the detector.
        :type pred_bboxes: ``list``

        :param pred_scores:
            list of :math:`N` sublists, each element the score 
            assigned to the prediciton by the face detector.
        :type pred_scores: ``list``

        :param \**kwargs:
            See below

        :Keyword Arguments:
            * **true_subset_bboxes** (``list``) --
                list of :math:`N` sublists, each element an index of a face in 
                ``true_bboxes`` that must not be ignored. 
              
            * **threshold_period** (``float``) --
                the number of periods in the PR Curve generation. Default is 1000.
              
            * **no_plot** (``bool``) --
                Set to `True` if plotting PR Curve is not required. Default is `False`.

            * **plot_dest** (``string``) --
                Set to some destination to save the plot. Not saved by default.

            * **plot_title** (``string``) --
                Give title to the PR Curve plot. 

        """

        # Ensure similar number of rows for the arguments
        assert(len(true_bboxes) == len(pred_bboxes) == len(pred_scores))
        self.true_bboxes = true_bboxes
        self.pred_bboxes = pred_bboxes
        self.pred_scores = self.scale_scores(pred_scores)

        self.true_subset_bboxes = kwargs.get('true_subset_bboxes', None)
        if self.true_subset_bboxes is not None:
            assert(len(self.true_subset_bboxes) == len(true_bboxes))

        self.threshold_period = kwargs.get('threshold_period', 1000)
        self.threshold_levels = np.linspace(1-1.0/self.threshold_period, 0, self.threshold_period)

        self.no_plot = kwargs.get('no_plot', False)
        self.plot_dest = kwargs.get('plot_dest', None)
        self.plot_title = kwargs.get('plot_title', '---')


    def _image_evaluate(self, true_bboxes, true_subset, pred_bboxes, pred_scores, iou_min=0.5):
        r"""
        Obtain score-thresholded values for the image detections.

        :param true_bboxes:
            Ground truth bounding boxes for the image.
        :type true_bboxes: ``list``

        :param true_subset:
            None if all ground truth faces are counted.
            Else list of indices of ground truth faces to count.
        :type true_subset: ``list``

        :param pred_bboxes:
            Predicted bounding boxes for the image.
        :type pred_bboxes: ``list``

        :param pred_scores:
            Scores for each predicted bounding box.
        :type pred_scores: ``list``

        :param iou_min:
            Bounding box IOU threshold for successful detection
        :type iou_min: ``float``


        :Returns:

            **minicurve_unignored_faces** (``list``)
                Number of unignored faces for some :math:`i` threshold. Removes faces
                correctly detected yet to be ignored.

            **minicurve_recalled_faces** (``list``)
                Number of recalled faces for some :math:`i` threshold.
        """


        true_bboxes = np.array(true_bboxes)
        pred_bboxes = np.array(pred_bboxes)
        true_bboxes[:,2:] = true_bboxes[:,2:] + true_bboxes[:,:2]
        pred_bboxes[:,2:] = pred_bboxes[:,2:] + pred_bboxes[:,:2]

        true_recalled = np.zeros(len(true_bboxes))  # 1 if true_bbox[i] is recalled, else 0
        cumulative_recall = np.zeros(len(pred_bboxes))
        unignored_faces = np.ones(len(pred_bboxes))  # 0 if face is ignored, else 1

        recalled = 0
        overlaps = bbox_overlaps(pred_bboxes.astype(np.float), true_bboxes.astype(np.float))

        for i in range(len(pred_bboxes)):
            overlap = overlaps[i]
            max_overlap, max_ind = overlap.max(), overlap.argmax()
            if max_overlap >= iou_min:
                if true_subset is not None and max_ind not in true_subset:
                    unignored_faces[i] = 0
                elif true_recalled[max_ind] == 0:  # multiple bounding boxes for same face
                    true_recalled[max_ind] = 1
                    recalled += 1
            cumulative_recall[i] = recalled

        cumulative_unignored_faces = np.cumsum(unignored_faces)

        minicurve_unignored_faces = np.empty(self.threshold_period)
        minicurve_recalled_faces = np.empty(self.threshold_period)
        neg_scores = -pred_scores

        for t in range(self.threshold_period):
            r_index = np.searchsorted(neg_scores, -self.threshold_levels[t], side='right')
            if r_index == 0:
                minicurve_unignored_faces[t] = 0
                minicurve_recalled_faces[t] = 0
            else:
                minicurve_unignored_faces[t] = cumulative_unignored_faces[r_index-1]
                minicurve_recalled_faces[t] = cumulative_recall[r_index-1]
        return minicurve_unignored_faces, minicurve_recalled_faces 

    def plot_pr_curve(self, pr_curve):
        r"""
        Plot PR Curve using ``matplotlib``. Saves or shows the plot depending on ``plot_dest``.

        :param pr_curve:
            An :math:`2\times N` dimensional list with ``pr_curve[0]`` being the precision
            and ``pr_curve[1]`` being the recall.
        :type pr_curve: ``list``

        """

        fig = plt.figure(0)
        plt.title('PR Curve : ' + self.plot_title)
        plt.axis([0, 1, 0, 1])
        plt.xlabel('Recall')
        plt.ylabel('Precision')
        plt.plot(pr_curve[1], pr_curve[0])
        if self.plot_dest is None:
            plt.show()
        else:
            plt.savefig(os.path.join(self.plot_dest, 'PR-Curve_' + self.plot_title + '.eps'))
        plt.close(fig)

    def _generate_pr_curve(self, curve_unignored_faces, curve_recalled_faces, true_faces_total):
        r"""
        Generate the PR Curve of the detections.

        :param curve_unignored_faces:
            Number of unignored faces for some :math:`i` threshold. Removes faces
            correctly detected yet to be ignored.
        :type curve_unignored_faces: ``list``


        :param curve_recalled_faces:
            Number of recalled faces for some :math:`i` threshold.
        :type curve_recalled_faces: ``list``

        :param true_faces_total:
            Total number of faces in the ground truth predictions: :math:`TP + FN`
        :type true_faces_total: ``int``

        :Returns:

            **pr_curve** (``list``)
                An :math:`2\times N` dimensional list with ``pr_curve[0]`` being the precision
                and ``pr_curve[1]`` being the recall.
                The plot is printed if `self.no_plot` is `False`.
        """

        pr_curve = np.zeros((2,  self.threshold_period))
        for i in range(self.threshold_period):
            pr_curve[0, i] = curve_recalled_faces[i] / curve_unignored_faces[i]  # TP / TP + FP
            pr_curve[1, i] = curve_recalled_faces[i] / true_faces_total  # TP / TP + FN

        if self.no_plot is False:
            self.plot_pr_curve(pr_curve)

        return pr_curve

    def average_precision(self, precision, recall):
        r"""
        Get the average precision given the precision and recall.

        :param precision:
            List with precision values for each threshold.
        :type precision: ``list``

        :param recall:
            List with recall values for each threshold.
        :type recall: ``list``

        :Returns:

            **ap** (``int``)
                The average precision given the precision and recall.
        """


        precision = np.concatenate(([0], precision, [0]))
        recall = np.concatenate(([0], recall, [1]))

        for i in range(len(precision) - 1, 0, -1):
            precision[i-1] = np.maximum(precision[i-1], precision[i])

        border_indices = np.where(recall[1:] != recall[:-1])[0]

        ap = np.sum((recall[border_indices + 1] - recall[border_indices]) * precision[border_indices + 1])
        return ap

    def scale_scores(self, scores):
        r"""
        Scale the scores to [0,1] range for min and max values.

        :param scores:
            A two-nested list with the scores for each detection.
        :type scores: ``list``

        :Returns:

            **scores** (``list``)
                The scaled scores of same dimension.
        """

        max_sc = max([max(image_scores) for image_scores in scores if len(image_scores) > 0])
        min_sc = min([min(image_scores) for image_scores in scores if len(image_scores) > 0])
        factor = max_sc - min_sc
        for i in range(len(scores)):
            for j in range(len(scores[i])):
                scores[i][j] = (scores[i][j] - min_sc) / factor
        return scores

    def evaluate(self):
        r"""
        Compute the PR Curve and the Average Precision for the detections.

        :Returns:

            **ap** (``float``)
               The Average Precision. 
        """

        curve_unignored_faces = np.zeros(self.threshold_period)
        curve_recalled_faces = np.zeros(self.threshold_period)
        true_faces_total = 0

        for i in range(len(self.pred_bboxes)):

            subset = None
            if self.true_subset_bboxes is not None:
                true_faces_total += len(self.true_subset_bboxes[i])
                subset = self.true_subset_bboxes[i]
            else:
                true_faces_total += len(self.true_bboxes[i])

            if len(self.pred_bboxes[i]) == 0 or len(self.true_bboxes) == 0:
                continue

            minicurve_unignored_faces, minicurve_recalled_faces = self._image_evaluate(self.true_bboxes[i], subset, self.pred_bboxes[i], self.pred_scores[i])
            curve_unignored_faces += minicurve_unignored_faces
            curve_recalled_faces += minicurve_recalled_faces

        pr_curve = self._generate_pr_curve(curve_unignored_faces, curve_recalled_faces, true_faces_total)
        ap = self.average_precision(pr_curve[0], pr_curve[1])
        print("Average Precision : ", ap)
        self.pr_curve = pr_curve
        return ap

