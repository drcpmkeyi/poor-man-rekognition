'''Implementation of FaceBoxes Face Detection using Tensorflow in FaceBoxes class'''

import os

import tensorflow as tf
import numpy as np

from ..base_detector import FaceDetector

class FaceBoxes(FaceDetector):

    r'''
    Performs face detection using a trained FaceBoxes model in Tensorflow.           
    '''

    def __init__(self, thresh=0.5, **kwargs):
        '''
        The Tensorflow session is also created here.

        :param thresh:
            Score threshold to consider a detection a face. By default 0.5. Ranges
            from 0 to 1.
        :type thresh: ``float``

        :param \**kwargs:
            See ``base_detector`` ``__init__()`` parameters
        '''

        super().__init__(**kwargs)

        self.thresh = thresh

        model_path = os.path.join(os.path.dirname(__file__), 'faceboxes_model.pb')

        with tf.gfile.GFile(model_path, 'rb') as f:
            graph_def = tf.GraphDef()
            graph_def.ParseFromString(f.read())
        graph = tf.Graph()
        with graph.as_default():
            tf.import_graph_def(graph_def, name='import')
        self.input_images = graph.get_tensor_by_name('import/image_tensor:0')
        self.output_ops = [
            graph.get_tensor_by_name('import/boxes:0'),
            graph.get_tensor_by_name('import/scores:0'),
            graph.get_tensor_by_name('import/num_boxes:0'),
        ]
        self.sess = tf.Session(graph=graph)

    def _single_detect(self, image):
        '''
        :Returns:

            **faces** (``list``)
                list of detected bounding boxes of the form :math:`[x,y,w,h]`.

            **scores** (``list``)
                list of scores for each detection. 

        See ``base_detector::_single_detect`` for more details.
        '''
        h, w, _ = image.shape
        image = np.expand_dims(image, 0)
        boxes, scores, num_boxes = self.sess.run(
            self.output_ops, feed_dict={self.input_images: image}
        )

        indices = scores[0] > self.thresh 
        boxes = boxes[0, indices]
        scores = scores[0, indices]

        scaler = np.array([h, w, h, w], dtype='float32')
        boxes = np.floor(boxes * scaler)
        boxes[:,2:] = boxes[:,2:] - boxes[:,:2]
        boxes[:,[0,1,2,3]] = boxes[:,[1,0,3,2]]
        return boxes.tolist(), scores

    def _detect(self, images, offset=None):
        '''
        .. note::
            All the input images **must** be of the same height and width. Appropriate scaling and
            resizing must be done beforehand.

        :Returns:

            **all_faces** (``list``)
                list of detected bounding boxes of the faces in all images for 
                :math:`[o, [x,y,w,h]]` where :math:`o` is the image index for the detected 
                face (added to ``offset``, if supplied).

            **all_scores** (``list``)
                list of scores of the detected faces in all the images.

        See ``base_detector::_single_detect`` for more details.
        '''
        if offset is None:
            offset = 0

        h, w, _ = images[0].shape

        boxes, scores, num_boxes = self.sess.run(
            self.output_ops, feed_dict={self.input_images: images}
        )

        all_faces = []
        all_scores = []
        scaler = np.array([h, w, h, w], dtype='float32')

        for ind in range(len(images)):

            indices = scores[ind] > self.thresh
            boxes_lp = np.floor(boxes[ind, indices] * scaler)
            boxes_lp[:,2:] = boxes_lp[:,2:] - boxes_lp[:,:2]
            boxes_lp[:,[0,1,2,3]] = boxes_lp[:,[1,0,3,2]]

            scores_lp = scores[ind, indices]

            for fc_ind in range(boxes_lp.shape[0]):
                all_faces.append([offset+ind, boxes_lp[fc_ind]])
                all_scores.append(scores_lp[fc_ind])

        return all_faces, all_scores
            
