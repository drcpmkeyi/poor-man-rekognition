"Implementation of ShotSegmentation base class"

from abc import abstractmethod, ABC

class ShotSegmentation(ABC):

    '''All segmentation methods must be a derived
    class of this ABC'''

    def __init__(self, filename):
        '''File to be segmented'''
        self.filename = filename

    @abstractmethod
    def _segment(self):
        '''
        Abstract method which returns the detected
        scenes and their start and stop frames
        '''
        pass

    def segment(self):
        '''
        Runner for _segment
        '''
        return self._segment()
