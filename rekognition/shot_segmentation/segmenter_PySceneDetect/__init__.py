'''PSDSegmenter, a shot segmentation method implementation based
on PySceneDetect'''

from scenedetect.video_manager import VideoManager
from scenedetect.scene_manager import SceneManager
from scenedetect.frame_timecode import FrameTimecode
from scenedetect.stats_manager import StatsManager
from scenedetect.detectors import ContentDetector

from ..base_segmenter import ShotSegmentation

class PSDSegmenter(ShotSegmentation):

    '''Performs shot segmentation using PySceneDetect'''

    def __init__(self, is_downscale=False, **kwargs):
        '''
        Initializes the PySceneDetect objects to perform
        the segmentation
        '''
        super().__init__(**kwargs)

        self.is_downscale = is_downscale

        self.video_manager = VideoManager([self.filename])
        self.stats_manager = StatsManager()
        self.scene_manager = SceneManager(self.stats_manager)
        self.scene_manager.add_detector(ContentDetector())

    def _segment(self):
        '''
        See ShotSegmentation._segment
        '''
        try:
            base_timecode = self.video_manager.get_base_timecode()
            if self.is_downscale:
                self.video_manager.set_downscale_factor()
            self.video_manager.start()
            self.scene_manager.detect_scenes(frame_source=self.video_manager)
            scene_list = self.scene_manager.get_scene_list(base_timecode)

            new_frame_start_locs = []
            for scene in scene_list:
                new_frame_start_locs.append((scene[0].get_frames(), scene[1].get_frames()-1))
        finally:
            self.video_manager.release()

        return new_frame_start_locs
