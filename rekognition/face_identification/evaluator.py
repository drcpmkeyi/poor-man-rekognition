"""Implementation of ``FaceEmbedderEvaluator`` class."""

import os

import numpy as np
from scipy.optimize import brentq
from scipy import interpolate
from sklearn import metrics
import matplotlib.pyplot as plt

class FaceEmbedderEvaluator:
    r"""Evaluate face embeddings for a database of faces.
    """

    def __init__(self, true_embeddings, pred_embeddings, is_same, **kwargs):
        """
        :param true_embeddings:
            list of :math:`N` sublists, each sublist containing the embeddings
            for the :math:`i^{th}` `first` face. 
        :type true_embeddings: ``list``

        :param pred_embeddings:
            list of :math:`N` sublists, each sublist containing the embeddings
            for the :math:`i^{th}` `second` face. 
        :type pred_embeddings: ``list``

        :param is_same:
            list of :math:`N` booleans, each boolean indicating whether the :math:`i^{th}` 
            face pairs are similar (True) or not.
        :type is_same: ``list``

        :param \**kwargs:
            See below

        :Keyword Arguments:
            * **threshold_max** (``float``) --
                Lowest threshold to start the threshold calculations from.
                Threshold signifies the distance (by default 0).

            * **threshold_min** (``float``) --
                Highest threshold to start the threshold calculations from.
                Threshold signifies the distance (by default 4).

            * **threshold_period** (``float``) --
                Threshold period for performing the evaluator calculations.
                Default is 0.01.
            
            * **no_plot** (``bool``) --
                Set to `True` if plotting ROC Curve is not required. Default is `False`.

            * **plot_dest** (``string``) --
                Set to some destination to save the plot. Not saved by default.

            * **plot_title** (``string``) --
                Give title to the ROC Curve plot. 

        """
        self.true_embeddings = true_embeddings
        self.pred_embeddings = pred_embeddings
        self.is_same = is_same

        self.threshold_max = 0
        self.threshold_min = 4
        self.threshold_period = 0.01

        self.no_plot = kwargs.get('no_plot', False)
        self.plot_dest = kwargs.get('plot_dest', None)
        self.plot_title = kwargs.get('plot_title', '---')

    def roc(self, true_emb, false_emb, is_same):
        """
        Obtain the ROC TP rate and FP rate, and draw the curve.

        :param true_emb:
            The left faces embeddings.
        :type true_emb: ``list``

        :param false_emb:
            The right faces embeddings.
        :type false_emb: ``list``

        :param is_same:
            List of booleans denoting if faces are of the same person.
        :type false_emb: ``list``

        :Returns:
            (``float``): True Positive Rate

            (``float``): False Positive Rate

            (``float``): Accuracy
        """

        thresholds = np.arange(self.threshold_max, self.threshold_min, self.threshold_period)

        diff = np.subtract(true_emb, false_emb)
        dist = np.sum(np.square(diff), 1)
        
        tpr = []
        fpr = []
        acc = 0

        for threshold in thresholds:
            prediction = np.less(dist, threshold)
            tp = np.sum(np.logical_and(prediction, is_same))
            fp = np.sum(np.logical_and(prediction, np.logical_not(is_same)))
            tn = np.sum(np.logical_and(np.logical_not(prediction), np.logical_not(is_same)))
            fn = np.sum(np.logical_and(np.logical_not(prediction), is_same))
            tpr_tsh = 0 if (tp + fn == 0) else float(tp) / float(tp + fn)
            fpr_tsh = 0 if (fp + tn == 0) else float(fp) / float(fp + tn)
            acc_tsh = float(tp + tn) / dist.size
            tpr.append(tpr_tsh)
            fpr.append(fpr_tsh)
            acc = max(acc, acc_tsh)

        if self.no_plot is False:
            self.plot_roc_curve(fpr, tpr)

        return tpr, fpr, acc

    def plot_roc_curve(self, fp, tp):
        r"""
        Plot ROC Curve using ``matplotlib``. Saves or shows the plot depending on ``plot_dest``.

        :param fp:
            List containing the FP rate 
        :type fp: ``list``

        :param tp:
            List containing the TP rate 
        :type tp: ``list``
        """

        fig = plt.figure(0)
        plt.title('ROC Curve : ' + self.plot_title)
        plt.axis([0, 1, 0, 1])
        plt.xlabel('False Positive')
        plt.ylabel('True Positive')
        plt.plot(fp, tp)
        if self.plot_dest is None:
            plt.show()
        else:
            plt.savefig(os.path.join(self.plot_dest, 'ROC-Curve_' + self.plot_title + '.png'), dpi=100)
        plt.close(fig)

    def evaluate(self):
        r"""
        Compute the quantification errors and metrics and plots the ROC curve.

        :Returns:

            **quantified_scores** (``dict``)
               Dictionary containing values of the various quantifying metrics.
        """
        
        quantified_scores = {}

        tpr, fpr, acc = self.roc(self.true_embeddings, self.pred_embeddings, self.is_same)
        quantified_scores['accuracy'] = acc
        quantified_scores['auc'] = metrics.auc(fpr, tpr)
        quantified_scores['eer'] = brentq(lambda x: 1. - x - interpolate.interp1d(fpr, tpr)(x), 0., 1.)

        return quantified_scores
