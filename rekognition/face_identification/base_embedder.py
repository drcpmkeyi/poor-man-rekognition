"""Contains the FaceEmbedder class for performing any Face
Embeddings generation."""

from abc import abstractmethod, ABC

import cv2
import numpy as np

class FaceEmbedder(ABC):
    """ Contains the abstract methods for finding face embeddings. Make sure
    to inherit this class when adding a new embedder.
    """

    def __init__(self, batch_size=10):
        '''
        :param batch_size:
            Batch size (number of images) to be processed in a single run. If a list
            of image names are passed, only ``batch_size`` amount will be read at a
            time. Default: 10.
        :type batch_size: ``list``
        '''
        self.batch_size = batch_size

    def generateEmbedding(self, src_images):
        '''
        Returns the embeddings of all images.

        :param src_images:
            List of all images containing faces to generate embeddings for
        :type src_images: ``list``
        '''
        embeddings = []
        offset = 0

        while offset < len(src_images):
            images = []
            lim = min(self.batch_size, len(src_images)-offset)
            for i in range(lim):
                images.append(src_images[offset+i])
            embeddings.extend(self._embed(images))
            offset += lim 

        return embeddings 

    @abstractmethod
    def _embed(self, images):
        '''
        Abstract method to generate embeddings of the images.
        '''
        pass
