.. Poor Man's Rekognition documentation master file, created by
   sphinx-quickstart on Sun Jun  9 22:50:45 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Poor Man's Rekognition's documentation!
==================================================

.. role:: raw-html(raw)
    :format: html

.. toctree::
   :maxdepth: 3
   :caption: Contents:

   face_detection
   face_alignment
   face_identification
   FRM
   VPM

.. toctree::
   :maxdepth: 2

   GSoC/index

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
