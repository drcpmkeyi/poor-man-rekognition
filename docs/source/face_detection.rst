``face_detection``
==================

.. automodule:: rekognition.face_detection
    :undoc-members:

-------------


``FaceDetector`` Base Class
---------------------------

*Also see:* :ref:`Face Detectors`

.. autoclass:: rekognition.face_detection.base_detector.FaceDetector

-------------

``FaceDetectorEvaluator`` Class
-------------------------------

Although WIDER FACE does come with an evaluator, it was necessary to add a general
evaluator to ease future development as well as usage of the library. First the PR Curve
is computed (using a user-configurable period length) on the normalized scores. The
Average Precision is calculated as defined in the Pascal VOC challenge: The precision
envelope is computed (from lower thresholds); this takes into account erroneous
variations. Then between each differing recall sample point, the (rectangular) area is 
calculated and summed to get the final AP.

.. autoclass:: rekognition.face_detection.FaceDetectorEvaluator

-------------

Face Detectors
--------------

A separate part of the ``face_detection`` submodule was the need to implement it in such
a way that future changes would be robust yet straightforward to implement. This is done in
two major ways:

- The first one is that any new detector ``xyz`` must be added as a derived class to the
  ABC (Abstract Base Class) ``FaceDetector`` (shown above). As the ``detect`` function is
  written in the ABC definition, the detector classes are instructed to base their code in
  ``_detect``, which has predefined parameters.
  
- The second one is that, with the addition, of any new Face Detector class, its PR Curve
  and AP scores must be measured for the validation sets of WIDER FACE (Easy, Medium, Hard).
  An example can be seen in the FaceBoxes section below. A Jupyter Notebook has been added
  in ``tests/performance/face_detection`` to run these with ease.

ViolaJones
````````````

.. note::
   Viola Jones was not performance tested using the evaluator due to the lack of scores being
   returned by the detector. However, as the scores aren't needed for the face recognition
   module, the ``ViolaJones`` detector can still be used (except for score-based
   evaluation).

*For more information, see:* `ViolaJones`_

.. _ViolaJones: https://www.cs.cmu.edu/~efros/courses/LBMV07/Papers/viola-cvpr-01.pdf

.. autoclass:: rekognition.face_detection.ViolaJones

FaceBoxes
````````````
======    ===================
Set       Average Precision
======    ===================
Easy      0.8296556509329163
Medium    0.7557988538989009
Hard      0.38497563366249976
======    ===================

Sequential speed: ~20 fps

.. image:: ../../tests/performance/face_detection/images/PRcurve_FaceBoxes.png

*For more information, see:* `FaceBoxes`_

.. _FaceBoxes: https://arxiv.org/pdf/1708.05234.pdf

.. autoclass:: rekognition.face_detection.FaceBoxes
