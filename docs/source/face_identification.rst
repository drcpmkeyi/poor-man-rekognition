``face_identification``
=======================

.. automodule:: rekognition.face_identification
    :undoc-members:

-------------

``FaceEmbedder`` Base Class
----------------------------

.. autoclass:: rekognition.face_identification.base_embedder.FaceEmbedder

-------------

``FaceEmbedderEvaluator`` Class
-------------------------------

The evaluator implements the standard values computed to compare various face
embeddings efficiency. This includes the accuracy, area under the curve (AUC) of the
ROC (CED) graph (which should be high) and the expected error rate (EER). The dataset 
used is the LFW dataset.

.. autoclass:: rekognition.face_identification.FaceEmbedderEvaluator

-------------

Face Embedder Methods
---------------------

The design philosophy here is similar to that of the Face Detection methods. The methods
must be added in a separate directory ``embedder_xyz`` as a derived class of ``FaceEmbedder``
class. The documentation should be supplied with the performance testing of the method too.
An example can be seen in MobileFaceNet documentation; the notebook to run this directly
can be seen in ``tests/performace/face_embedder``.

MobileFaceNet
`````````````

=============    ===================
Metric           Value 
=============    ===================
Accuracy          0.7255 
AUC               0.789            
EER               0.279           
=============    ===================

Sequential speed: ~25 fps

.. image:: ../../tests/performance/face_embedder/images/ROC-Curve_MobileFaceNet.png

*For more information, see:* `MobileFaceNet`_

.. _MobileFaceNet: https://arxiv.org/abs/1804.07573 

.. autoclass:: rekognition.face_identification.MobileFaceNet


--------------

``FaceIdentifier`` Class
------------------------

The wrapper around this submodule is the ``FaceIdentifier`` class which, apart from the
embedders, also wraps around a ``FaceKNN`` class which performs the KNN training and
testing explicitly. ``FaceIdentifier`` accepts a custom Face Embedder as an argument.

.. autoclass:: rekognition.face_identification.FaceIdentifier
