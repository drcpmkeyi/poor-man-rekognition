``face_alignment``
==================

.. automodule:: rekognition.face_alignment
    :undoc-members:

-------------

``FaceAlignment`` Base Class
----------------------------

.. autoclass:: rekognition.face_alignment.base_alignment.FaceAlignment

-------------

``FaceAlignmentEvaluator`` Class
--------------------------------

The evaluator implements the standard values computed to compare various face
alignment methods. This includes the average error, area under the curve of the
Cumulative Error Distribution (CED) graph (which should be high) and the failure
rate (fraction of faces that were falsely landmarked). The dataset used is a custom
135 images set (used in 300-W challenge) called iBUG. This is a very challenging
set with high failure rates. We took this dataset as with time the alignment methods
will improve, and the failure rate on this set would decrease; but it can be used
for quite a while. MENPO was rejected due to issues in the challenge format - no
bounding boxes would be provided for the face.

.. autoclass:: rekognition.face_alignment.FaceAlignmentEvaluator

-------------


Face Alignment Methods
----------------------

The design philosophy here is similar to that of the Face Detection methods. To
concisely reiterate, the methods must be added in a separate directory ``alignment_xyz``
as a derived class of ``FaceAlignment`` class. The documentation should be supplied
with the performance testing of the method too. An example can be seen in the Kazemi
Sullivan documentation; the notebook to run this directly can be seen in ``test/performance/face_alignment``.

Kazemi Sullivan
````````````````

.. note::
   ``KazemiSullivan`` needs to be provided with a model, which is not included with the
   module due to the large size. However, one such model has been added as an LFS file
   in ``tests/models/KazemiSullivanModel.dat``.

=============    ===================    ============    =======
Normalization    Average Error (%)      Failure Rate    AUC
=============    ===================    ============    =======
Corners           19.7                   0.6             15.367 
Diagonal          7.1                    0.355           39.21
Pupil             28.4                   0.7481          7.126
=============    ===================    ============    =======

.. image:: ../../tests/performance/face_alignment/images/CED-Curve_KazemiSullivan.png

Sequential speed: ~400 landmark detections per second (on CPU)

*For more information, see:* `KazemiSullivan`_

.. _KazemiSullivan: http://www.nada.kth.se/~sullivan/Papers/Kazemi_cvpr14.pdf 

.. autoclass:: rekognition.face_alignment.KazemiSullivan

NoAlignment
```````````

.. note::
   This is `not` an alignment method. For maintaining an straightforward pipeline, this
   ``NoAlignment`` derived `FaceAlignment` class only performs the resizing, as in some
   situations the alignment procedure is not favoured.

.. autoclass:: rekognition.face_alignment.NoAlignment
